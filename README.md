# rest-demo

This is a simple REST API implemented in Clojure with the following endpoints:
* /
* /request - Shows request output
* /hello?name=JonDoe -  Outputs Hello [namevalue]
* /people - Outputs JSON list of stored people
* /people/add?firstname=Jon&surname=Doe - Adds person using given firstname/surname parameters

## Principles Displayed
This example shows a simple usage of routing, JSON manipulation, using Atoms for storage, partially applying a function, and writing event handlers for routes.

## Building and Testing Yourself
To build:
1.  Clone this repo
2.  Cd into the project directory.
3.  Ensure you have Leiningen installed, and run

        lein run

4.  View the example at localhost:3000/[endpoint] in your browser.
